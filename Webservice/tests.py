# from django.test import TestCase
# from django.test import Client
# from django.urls import resolve
# from django.http import HttpRequest
# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options
# from django.db import IntegrityError
# import unittest
# import time

# from .views import subscribe
# from .views import checkEmail
# from .models import Subscriber
# from .forms import SubscriberForm
# # Create your tests here.

# class Lab10UnitTest(TestCase):
#     def test_webservice_url_is_exist(self):
#         response = Client().get('/')
#         self.assertEqual(response.status_code, 200)
    
#     def test_Apibooks_url_is_exist(self):
#         response = Client().get('check')
#         self.assertEqual(response.status_code, 404)

#     def test_webservice_using_status_form_template(self):
#         response = Client().get('/')
#         self.assertTemplateUsed(response, 'webservicehomepage.html')
    
#     def test_webservice_using_make_status_func(self):
#         found = resolve('/')
#         self.assertEqual(found.func, subscribe)

#     def test_webservice_using_make_status_func_check(self):
#         found = resolve('/check-email')
#         self.assertEqual(found.func, checkEmail)

#     def test_model_can_create_new_subscriber(self):
#         new_subscriber = Subscriber.objects.create(name="aku", email="kamu@yahoo.com", password="hello321654")
#         counting_all_subscriber = Subscriber.objects.all().count()
#         self.assertEqual(counting_all_subscriber, 1)

#     def test_FormSubscriber_valid(self):
#         form = SubscriberForm(data={'name': "me", 'email': "user@yahoo.com" , 'password': "user1234"})
#         self.assertTrue(form.is_valid())
    
#     def test_max_length_name(self):
#         name = Subscriber.objects.create(name="not more than 30 character")
#         self.assertLessEqual(len(str(name)), 30)
    
#     def test_unique_email(self):
#         Subscriber.objects.create(email="tepe@email.com")
#         with self.assertRaises(IntegrityError):
#             Subscriber.objects.create(email="tepe@email.com")
        
#     def test_check_email_view_get_return_200(self):
#         email = "tepe@gmail.com"
#         Client().post('/check-email/', {'email': email})
#         response = Client().post('/', {'email': 'tepe@gmail.com'})
#         self.assertEqual(response.status_code, 200)

#     def test_check_email_already_exist_view_get_return_200(self):
#         Subscriber.objects.create(name="dia", email="kau@gmail.com", password="edcba")
#         response = Client().post('/check-email/', {
#             "email": "kau@gmail.com"
#         })
#         self.assertEqual(response.json()['is_email'], True)

#     def test_subscribe_should_return_status_subscribe_true(self):
#         response = Client().post('/', {
#             "name": "dia",
#             "email": "anda@gmail.com",
#             "password":  "logueendnow",
#         })
#         self.assertEqual(response.json()['status_subscribe'], True)

#     def test_subscribe_should_return_status_subscribe_false(self):
#         name, email, password = "tepe", "tepe@gmail.com", "password"
#         Subscriber.objects.create(name=name, email=email, password=password)
#         response = Client().post('/', {
#             "name": name,
#             "email": email,
#             "password":  password,
#         })
#         self.assertEqual(response.json()['status_subscribe'], False)


# class Lab10FunctionalTest(unittest.TestCase):
#         def setUp(self):
#                 chrome_options = Options()
#                 chrome_options.add_argument('--dns-prefetch-disable')
#                 chrome_options.add_argument('--no-sandbox')
#                 chrome_options.add_argument('--headless')
#                 chrome_options.add_argument('disable-gpu')
#                 service_log_path = "./chromedriver.log"
#                 service_args = ['--verbose']
#                 self.selenium  = webdriver.Chrome(
#                         './chromedriver', chrome_options=chrome_options
#                 )
#                 super(Lab10FunctionalTest, self).setUp()

#         def tearDown(self):
#                 self.selenium.quit()
#                 super(Lab10FunctionalTest, self).tearDown()

#         def test_content(self):
#             selenium = self.selenium

#             selenium.get('http://localhost:8000/')
