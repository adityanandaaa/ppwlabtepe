from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time

from .views import lab9_json
# Create your tests here.
# hasem baem
# baem baem banaleknangalk

class Lab9UnitTest(TestCase):
    def test_AjaxWeb_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_Apibooks_url_is_exist(self):
        response = Client().get('data')
        self.assertEqual(response.status_code, 404)

    def test_AjaxWeb_using_status_form_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'ajaxhomepage.html')
    
    def test_AjaxWeb_using_make_status_func(self):
        found = resolve('/')
        self.assertEqual(found.func, lab9_json)

    def test_Apibooks_using_make_status_func(self):
        found = resolve('/')
        self.assertEqual(found.func, lab9_json)

class Lab9FunctionalTest(unittest.TestCase):
        def setUp(self):
                chrome_options = Options()
                chrome_options.add_argument('--dns-prefetch-disable')
                chrome_options.add_argument('--no-sandbox')
                chrome_options.add_argument('--headless')
                chrome_options.add_argument('disable-gpu')
                service_log_path = "./chromedriver.log"
                service_args = ['--verbose']
                self.selenium  = webdriver.Chrome(
                        './chromedriver', chrome_options=chrome_options
                )
                super(Lab9FunctionalTest, self).setUp()

        def tearDown(self):
                self.selenium.quit()
                super(Lab9FunctionalTest, self).tearDown()

        def test_content(self):
            selenium = self.selenium

            selenium.get('http://localhost:8000/')

            content1 = selenium.find_element_by_tag_name('h2') #tulisan counter
            content2 = selenium.find_element_by_id('number').text #table number
            content3 = selenium.find_element_by_id('image').text #table image
            content4 = selenium.find_element_by_id('title').text #table title
            content5 = selenium.find_element_by_id('author').text #table author
            # content6 = selenium.find_element_by_id('publisher').text #table publisher
            content7 = selenium.find_element_by_id('publishdate').text #table publish date
            images = selenium.find_elements_by_tag_name('img') #ambil foto
            # for image in images:
            #     print(image.get_attribute('src'))
            # content8 = selenium.find_element_by_id('star').value_of_css_property('color') #ambil bintang kejora
            content9 = selenium.find_element_by_id('starcounter').value_of_css_property('color') #ambil bintang kejora
            # content10 = selenium.find_element_by_id('tablefont').value_of_css_property('font-size') #ambil fontsize table
            content11 = selenium.find_element_by_tag_name('table').value_of_css_property('font-family') #ambil font-family
            content12 = selenium.find_element_by_tag_name('th').value_of_css_property('font-family') #ambil font-family
            content13 = selenium.find_element_by_tag_name('p').text #kata mutiara
            content14 = selenium.find_element_by_id('pagecount').text

            

            self.assertIn('Just some star counter',selenium.page_source)
            self.assertIn('No',content2)
            self.assertIn('Image',content3)
            self.assertIn('Book Title',content4)
            self.assertIn('Authors',content5)
            # self.assertIn('Publisher',content6)
            self.assertIn('Published Date',content7)
            # self.assertIn('rgba(255, 255, 255, 1)',content8)
            self.assertIn('rgba(99, 23, 49, 1)',content9)
            # self.assertIn('20px',content10)
            self.assertIn('Ubuntu',content11)
            self.assertIn('Ubuntu',content12)
            self.assertIn('"Sebuah kata-kata Mutiara"',content13)
            self.assertIn('Tepe',selenium.title)
            self.assertIn('Page Count',content14)