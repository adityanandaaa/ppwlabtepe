from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
import json
import requests
from .models import Subscriber
from .forms import SubscriberForm
# Create your views here.

response={}
def subscribe(request):
    response['activetab'] = 'subscriberpage'
    if request.method == 'POST':
        form = SubscriberForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            status_subscribe = True
            try:
                Subscriber.objects.create(name=name, email=email, password=password)
            except:
                status_subscribe = False
            return JsonResponse({'status_subscribe' : status_subscribe})
    Form = SubscriberForm()
    response['form'] = Form
    return render(request, 'webservicehomepage.html', response)

def checkEmail(request):
    if request.method == 'POST':
        email = request.POST['email']
        validasi = Subscriber.objects.filter(pk=email).exists()
        return JsonResponse({'is_email' : validasi})


