from django import forms

class JadwalForm(forms.Form):
    nama_acara = forms.CharField(label = "Name", widget=forms.TextInput(attrs={'size':'80'}))
    hari_acara = forms.CharField(label = "Day" ,widget=forms.TextInput(attrs={'size':'82'}))
    tanggal_acara = forms.DateField(label = "Date", widget = forms.DateInput(attrs ={'type' : 'date'}))
    waktu_acara = forms.TimeField(label = "Time", widget = forms.TimeInput(attrs ={'type' : 'time'}))
    tempat_acara = forms.CharField(label = "Venue", widget=forms.TextInput(attrs={'size':'80'}))
    kategori_acara = forms.CharField(label = "Category", widget=forms.TextInput(attrs={'size':'79'}))
    
    
