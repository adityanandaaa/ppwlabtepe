from django.contrib import admin
from django.urls import path
from django.conf.urls import url
from django.http import HttpResponse
from django.contrib import admin

from . import views
from .views import InputForm
from .views import PostForm
from .views import Form
from .views import DeleteView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.Home, name ='home'),
    url(r'^profile/', views.Profile, name = 'profile'),
    url(r'^regis/', views.Regis, name = 'regis'),
    url(r'^inputform/', InputForm, name = 'InputForm'),
    url(r'^form/', Form, name = 'Form'),
    url(r'^postform/', PostForm, name = 'PostForm'),
    url(r'^deleteview/', DeleteView, name = 'DeleteView'),
]
