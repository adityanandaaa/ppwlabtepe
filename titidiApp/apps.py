from django.apps import AppConfig


class TitidiappConfig(AppConfig):
    name = 'titidiApp'
