# from django.test import TestCase
# from django.test import Client
# from django.urls import resolve
# from django.http import HttpRequest
# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options
# import unittest

# from .views import make_status
# from .views import Profile
# from .models import Status
# from .forms import StatusForm
# # Create your tests here.

# class Lab7FunctionalTest(unittest.TestCase):
#         def setUp(self):
#                 chrome_options = Options()
#                 chrome_options.add_argument('--dns-prefetch-disable')
#                 chrome_options.add_argument('--no-sandbox')
#                 chrome_options.add_argument('--headless')
#                 chrome_options.add_argument('disable-gpu')
#                 service_log_path = "./chromedriver.log"
#                 service_args = ['--verbose']
#                 self.selenium  = webdriver.Chrome(
#                         './chromedriver', chrome_options=chrome_options
#                 )
#                 super(Lab7FunctionalTest, self).setUp()

#         def tearDown(self):
#                 self.selenium.quit()
#                 super(Lab7FunctionalTest, self).tearDown()

#         def test_input_todo(self):
#                 selenium = self.selenium

#                 #selenium.get('http://localhost:8000/titidi/')
#                 selenium.get('https://ppw-b-ppwlabpunyatepe.herokuapp.com/titidi/')   #yang nongolin pas test
#                 #driver = webdriver.Chrome()

#                 #find html tag and else
#                 formstatus = selenium.find_element_by_id('id_isi_status')
#                 submit = selenium.find_element_by_name('post').text
#                 header_text = selenium.find_element_by_tag_name('h1').text
#                 #find css selector
#                 content1 = selenium.find_element_by_css_selector('body.bodytitidi').value_of_css_property('background-color')
#                 content2 = selenium.find_element_by_css_selector('input.rounded-edge').value_of_css_property('border-radius')
#                 #masukin form
#                 formstatus.send_keys('Atta Halilintar')
#                 formstatus.send_keys(Keys.ENTER)
#                 #cek isi css selector
#                 self.assertIn('rgba(255, 255, 255, 1)', content1)
#                 self.assertIn('13.3333px', content2)
#                 #cek isi tag selector
#                 self.assertIn('Update Status disokin',selenium.page_source)                                             #title
#                 self.assertIn('Hello,apa kabar semua kembali lagi dichannel terkece,terkeren,aahsiaaap',header_text)    #H1
#                 self.assertIn('Atta Halilintar', selenium.page_source)                                                  #Hasil isi
#                 self.assertIn('post',selenium.page_source)                                                              #Tombol post
#                 #self.assertIn('Post',submit)        yang ini gabisa karena di htmlnya saya pake value                  #Tombol post
                

# #if __name__ == '__main__':
#  #       unittest.main(warnings='ignore')

# class Lab6UnitTest(TestCase):
#     def test_titidi_url_is_exist(self):
#             response = Client().get('Status')
#             self.assertEqual(response.status_code, 404)

#     def test_titidi_using_status_form_template(self):
#             response = Client().get('/titidi/')
#             self.assertTemplateUsed(response, 'StatusHome.html')

#     def test_titidi_using_make_status_func(self):
#             found = resolve('/titidi/')
#             self.assertEqual(found.func, make_status)

#     def test_model_can_create_new_status(self):
#             # Creating a new status
#             new_status = Status.objects.create(isi_status = 'Ngopi')

#              # Retrieving all available status
#             counting_status = Status.objects.all().count()
#             self.assertEqual(counting_status, 1)

#     def test_form_validation_for_blank_items(self):
#             form = StatusForm(data={'isi_status' : ''})
#             self.assertFalse(form.is_valid())
#             self.assertEqual(
#                 form.errors['isi_status'],
#                 ["This field is required."]
#             )
#     def test_lab6_post_success_and_render_the_result(self):
#             test = ''
#             response_post = Client().post('Status', {'isi_status': test})
#             self.assertEqual(response_post.status_code, 404)
    
#             response= Client().get('Status')
#             html_response = response.content.decode('utf8')
#             self.assertIn(test, html_response)

#     def test_lab6_post_error_and_render_the_result(self):
#             test = 'Anonymous'
#             response_post = Client().post('Status', {'isi_status': test})
#             self.assertEqual(response_post.status_code, 404)
    
#             response= Client().get('Status')
#             html_response = response.content.decode('utf8')
#             self.assertNotIn(test, html_response)

#     def test_profile_url_is_exist(self):
#             response = Client().get('Profile')
#             self.assertEqual(response.status_code, 404)

#     def test_profile_using_profile_page_template(self):
#             response = Client().get('/profilepage/')
#             self.assertTemplateUsed(response, 'profilepage.html')

#     def test_profile_using_profile_func(self):
#             found = resolve('/profilepage/')
#             self.assertEqual(found.func, Profile)
            
#     def test_can_save_a_POST_request(self):
#             response = self.client.post('/titidi/', data={'isi_status': 'test'})
#             counting_all_available_status = Status.objects.all().count()
#             self.assertEqual(counting_all_available_status, 1)

#             self.assertEqual(response.status_code, 302)
#             self.assertEqual(response['location'], '/titidi/')

#             new_response = self.client.get('/titidi/')
#             html_response = new_response.content.decode('utf8')
#             self.assertIn('test', html_response)




   



    


