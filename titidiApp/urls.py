from django.contrib import admin
from django.urls import path
from django.conf.urls import url
from django.http import HttpResponse


from . import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^titidi/', views.make_status, name ='Status'),
    url(r'^profilepage/', views.Profile, name='Profile'),
    
]