from django.contrib import admin
from django.urls import path
from django.conf.urls import url
from django.http import HttpResponse
from django.contrib import admin

from . import views
#from .views import Home

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.Home, name ='home'),
]