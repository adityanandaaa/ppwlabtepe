from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate
import json
import requests
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
response = {}

def buku(request):
    try: 
        search = request.GET["search"]
    except:
        search = "quilting"

    getBooksJson = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + search)   
    jsonParsed = json.dumps(getBooksJson.json())
    return HttpResponse(jsonParsed)

def lab9_json(request):
    return render(request,'ajaxhomepage.html')

def index(request):
    if request.user.is_authenticated and "like" not in request.session:
        request.session["fullname"] = request.user.first_name + \
            " "+request.user.last_name
        request.session["username"] = request.user.username
        request.session["email"] = request.user.email
        request.session["sessionid"] = request.session.session_key
        request.session["like"] = []
    return render(request, 'ajaxhomepage.html')


@csrf_exempt
def like(request):
    if(request.method == "POST"):
        lst = request.session["like"]
        if request.POST["id"] not in lst:
            lst.append(request.POST["id"])
        print(request.session["sessionid"])
        print(request.session["like"])
        request.session["like"] = lst
        response["message"] = len(lst)
        return JsonResponse(response)
    else:
        return HttpResponse("GET Method not allowed")

@csrf_exempt
def unlike(request):
    if(request.method == "POST"):
        lst = request.session["like"]
        if request.POST["id"] in lst:
            lst.remove(request.POST["id"])
        print(request.session["sessionid"])
        print(request.session["like"])
        request.session["like"] = lst
        response["message"] = len(lst)
        return JsonResponse(response)
    else:
        return HttpResponse("GET Method not allowed")

def get_like(request):
    if request.user.is_authenticated:
        if(request.method == "GET"):
            if request.session["like"] is not None:
                response["message"] = request.session["like"]
        else:
            response["message"] = "NOT ALLOWED"
    else:
        response["message"] = ""
    return JsonResponse(response)




    # raw_data = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting').json()
    # itemsmuch = []
    # for info in raw_data['items']:
    #     data = {}
    #     data['image'] = info['volumeInfo']['imageLinks']['thumbnail']
    #     data['title'] = info['volumeInfo']['title']
    #     data['author'] = " and ".join(info['volumeInfo']['authors'])
    #     data['publishedDate'] = info['volumeInfo']['publishedDate']
    #     data['pageCount'] = info['volumeInfo']['pageCount']
    #     itemsmuch.append(data)
    # return JsonResponse({"data" :itemsmuch})

