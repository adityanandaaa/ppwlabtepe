from django.db import models
from django.utils import timezone
from datetime import datetime, date



# Create your models here.


class Jadwal(models.Model):
    nama_acara = models.CharField(max_length=30)
    hari_acara = models.CharField(max_length=30) 
    tanggal_acara = models.DateField()
    waktu_acara = models.TimeField()
    tempat_acara = models.CharField(max_length=30)
    kategori_acara = models.CharField(max_length=30)

    
