from django.contrib import admin
from django.urls import path
from django.conf.urls import url,include
from django.http import HttpResponse
from django.contrib.auth import views
from django.contrib.auth import views as auth_views
from django.conf import settings


from . import views
#koniciwa

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.lab9_json, name ='home'),
    url(r'^login/$', auth_views.LoginView.as_view(), name='login'),
    url(r'^logout/$', auth_views.LogoutView.as_view(), {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout'),
    url(r'^auth/', include('social_django.urls', namespace='social')),
    url('data',views.buku,name="buku"),
    path('like/', views.like),
    path('unlike/', views.unlike),
    path('get-like/', views.get_like),

]