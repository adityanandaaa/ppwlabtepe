from django import forms


class SubscriberForm(forms.Form):
    name = forms.CharField(required=True, max_length=30, widget=forms.TextInput(attrs={'label': 'Name', 'id' : 'name'}))
    email = forms.CharField(required=True, max_length=30, widget=forms.TextInput(attrs={'type': 'email', 'placeholder':'example@email.com', 'label': 'Email', 'id' : 'email'}))
    password = forms.CharField(required=True, min_length=8, max_length=15, widget=forms.TextInput(attrs={'type': 'password', 'label': 'Password', 'id' : 'password'}))

    

