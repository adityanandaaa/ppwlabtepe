from django.contrib import admin
from django.urls import path
from django.conf.urls import url
from django.http import HttpResponse

from . import views


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.subscribe, name ='shows'),
    url(r'^check-email', views.checkEmail, name ='check'),
    
]