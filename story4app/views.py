from django.shortcuts import render
from .forms import JadwalForm
from .models import Jadwal
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic.edit import DeleteView

# Create your views here.

response = { 'author' : 'Aditya Nanda'}

def Home(request):
    return render(request,'homepage.html')

def Profile(request):
    return render(request,'profilepage.html')

def Regis(request):
    return render(request, 'registration.html')

def InputForm(request):
    html = "jadwalform.html"
    response["jadwalform"] = JadwalForm
    return render(request, html, response)

def PostForm(request):
    form = JadwalForm(request.POST or None)
    if(request.method == "POST"):
        response["nama_acara"] = request.POST["nama_acara"]
        response["hari_acara"] = request.POST["hari_acara"]
        response["tanggal_acara"] = request.POST["tanggal_acara"]
        response["waktu_acara"] = request.POST["waktu_acara"]
        response["tempat_acara"] = request.POST["tempat_acara"]
        response["kategori_acara"] = request.POST["kategori_acara"]

        jadwal_pribadi = Jadwal(nama_acara = response["nama_acara"], hari_acara = response["hari_acara"], tanggal_acara = response["tanggal_acara"], waktu_acara = response["waktu_acara"], tempat_acara = response["tempat_acara"], kategori_acara = response["kategori_acara"])
        jadwal_pribadi.save()
        html= "hasilform.html"
        jadwal_object = Jadwal.objects.all()
        response['jadwal_object'] = jadwal_object
        return render(request, html, response)

    else:
        return HttpResponseRedirect('/')

def Form(request):
        response["jadwal_dict"] = Jadwal.objects.all().values()
        return render(request, "hasilform.html" ,response)

def DeleteView(request):
        jadwal_object = Jadwal.objects.all().delete()
        response["jadwal_object"] = jadwal_object
        return render(request, "hasilform.html" ,response)
        

